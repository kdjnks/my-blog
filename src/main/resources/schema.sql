DROP TABLE IF EXISTS POSTS;
DROP TABLE IF EXISTS USERS;

CREATE TABLE USERS (
    user_id SERIAL PRIMARY KEY,
    user_name VARCHAR(40) NOT NULL,
    register_date DATE NOT NULL
);

CREATE TABLE POSTS (
    post_id SERIAL PRIMARY KEY,
    post_name VARCHAR(40) NOT NULL,
    post_description VARCHAR(120) NOT NULL,
    post_date DATE not null,
    author_id INTEGER NOT NULL,
    FOREIGN KEY (author_id) REFERENCES USERS(user_id)
);

ALTER SEQUENCE users_user_id_seq RESTART WITH 3;

INSERT INTO USERS(user_name, register_date)
VALUES ('K_D', NOW());

INSERT INTO POSTS(post_name, post_description, post_date, author_id)
VALUES ('Post Test 1', 'Simple description 1', NOW() , 1);

select * from posts;
select * from users;

truncate POSTS;
truncate USERS;
