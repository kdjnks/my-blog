package com.kd.blog.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static com.kd.blog.entity.User.*;

@NamedNativeQueries({
        @NamedNativeQuery(name = FIND_ALL,
                query = "select * from users",
                resultClass = User.class),
        @NamedNativeQuery(name = FIND_USER_BY_NAME,
                query = "select * from users u where u.user_name = ?1",
                resultClass = User.class),
        @NamedNativeQuery(name = CREATE_USER,
                query = "insert into users(user_name, register_date) " +
                        "values (?1, ?2) returning user_id")
})

@Entity
@Table(name = "users")
public class User {

    public static final String FIND_ALL = "User.findAll";
    public static final String FIND_USER_BY_NAME = "User.findUserByName";
    public static final String CREATE_USER = "User.createUser";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_id")
    private Long id;

    @Column(name = "user_name")
    private String name;

    @Column(name = "register_date")
    @Temporal(TemporalType.DATE)
    private Date registerDate;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "author")
    private List<Post> posts;

    public User() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}
