package com.kd.blog.entity;

import javax.persistence.*;

import java.util.Date;

import static com.kd.blog.entity.Post.FIND_ALL_POSTS;
import static com.kd.blog.entity.Post.FIND_POSTS_BY_USER;
import static com.kd.blog.entity.Post.INSERT_NEW_POST;

@NamedNativeQueries({
        @NamedNativeQuery(name = FIND_ALL_POSTS,
            query = "select * from posts p " +
                    "inner join users u on u.user_id = p.author_id",
            resultClass = Post.class),
        @NamedNativeQuery(name = FIND_POSTS_BY_USER,
                query = "select * from posts p " +
                        "where p.author= ?1",
                resultClass = Post.class),
        @NamedNativeQuery(name = INSERT_NEW_POST,
                query = "insert into posts(post_name, post_description, author_id, post_date)" +
                        "values(?1,?2,?3,?4) ",
                resultClass = Post.class)
})

@Entity
@Table(name = "posts")
public class Post {

    public static final String FIND_ALL_POSTS = "Post.findAll";
    public static final String FIND_POSTS_BY_USER = "Post.findPostsByUser";
    public static final String INSERT_NEW_POST = "Post.insertNewPost";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="post_id")
    private Long id;

    @Column(name = "post_name")
    private String name;

    @Column(name = "post_description")
    private String description;

    @Column(name = "post_date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "author_id")
    private User author;

    public Post() {
        //required by Hibernate
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
