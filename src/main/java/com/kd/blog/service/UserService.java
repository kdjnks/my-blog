package com.kd.blog.service;

import com.kd.blog.dao.UserDao;
import com.kd.blog.dto.UserDto;
import com.kd.blog.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserService {

    private final UserDao userDao;

    @Autowired
    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public UserDto findUserByName(String name) {
        User user = userDao.findUserByName(name);

        return Objects.nonNull(user) ? new UserDto.Builder()
                .name(user.getName())
                .registerDate(user.getRegisterDate())
                .build() : null;
    }
}
