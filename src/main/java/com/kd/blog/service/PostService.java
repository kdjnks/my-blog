package com.kd.blog.service;

import com.kd.blog.dao.PostDao;
import com.kd.blog.dao.UserDao;
import com.kd.blog.dto.PostDto;
import com.kd.blog.dto.UserDto;
import com.kd.blog.entity.Post;
import com.kd.blog.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PostService {

    private final UserDao userDao;
    private final PostDao postDao;

    @Autowired
    public PostService(UserDao userDao, PostDao postDao) {
        this.userDao = userDao;
        this.postDao = postDao;
    }

    public int createPost(PostDto post) {
        if(Objects.isNull(post)) {
            return 0;
        }

        return postDao.createPost(post.getName(), post.getDescription(),
                Optional.ofNullable(userDao.findUserByName(post.getAuthor().getName()))
                        .map(User::getId)
                        .orElse((long) userDao.createUser(post.getAuthor().getName())));
    }

    public List<PostDto> findAll() {
        List<Post> posts = postDao.findAll();

        return posts.stream()
                .map(post ->
                        new PostDto.Builder()
                                .name(post.getName())
                                .description(post.getDescription())
                                .date(post.getDate())
                                .author(
                                        new UserDto.Builder()
                                                .name(post.getAuthor().getName())
                                                .registerDate(post.getAuthor().getRegisterDate())
                                                .build())
                                .build())
                .collect(Collectors.toList());
    }
}
