package com.kd.blog.controller;

import com.kd.blog.dto.UserDto;
import com.kd.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@Controller
@RequestMapping(value = "/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{name}")
    public @ResponseBody ResponseEntity<UserDto> findUserByName(@PathVariable String name) {
        return ResponseEntity.ok(userService.findUserByName(name));
    }
}
