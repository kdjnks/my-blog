package com.kd.blog.controller;

import com.kd.blog.dto.PostDto;
import com.kd.blog.dto.UserDto;
import com.kd.blog.entity.Post;
import com.kd.blog.service.PostService;
import com.kd.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@Controller
@RequestMapping(value = "/posts")
public class PostController {

    private final PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping
    public @ResponseBody ResponseEntity<List<PostDto>> findAll() {
        return ResponseEntity.ok(postService.findAll());
    }

    @PostMapping
    public @ResponseBody ResponseEntity<String> createPost(@RequestBody PostDto post) {
        return ResponseEntity.ok(postService.createPost(post) == 1 ?
                String.format("Post %s was created.", post.getName()) :
                "Post was not created.");
    }
}
