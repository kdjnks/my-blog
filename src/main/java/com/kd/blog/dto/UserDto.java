package com.kd.blog.dto;

import java.util.Date;

public class UserDto {

    private String name;
    private Date registerDate;

    private UserDto(String name, Date registerDate) {
        this.name = name;
        this.registerDate = registerDate;
    }

    public String getName() {
        return name;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public static class Builder {

        private String name;
        private Date registerDate;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder registerDate(Date date) {
            this.registerDate = date;
            return this;
        }

        public UserDto build() {
            return new UserDto(name, registerDate);
        }
    }
}
