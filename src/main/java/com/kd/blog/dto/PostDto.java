package com.kd.blog.dto;

import java.util.Date;

public class PostDto {

    private String name;
    private String description;
    private UserDto author;
    private Date date;

    private PostDto(String name, String description, UserDto author, Date date) {
        this.name = name;
        this.description = description;
        this.author = author;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public UserDto getAuthor() {
        return author;
    }

    public Date getDate() {
        return date;
    }

    public static class Builder {

        private String name;
        private String description;
        private UserDto author;
        private Date date;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder author(UserDto author) {
            this.author = author;
            return this;
        }

        public Builder date(Date date) {
            this.date = date;
            return this;
        }

        public PostDto build() {
            return new PostDto(name, description, author, date);
        }
    }
}
