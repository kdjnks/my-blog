package com.kd.blog.dao;

import com.kd.blog.entity.User;

public interface UserDao {

    User findUserByName(String name);

    int createUser(String name);

}
