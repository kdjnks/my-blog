package com.kd.blog.dao.impl;

import com.kd.blog.dao.UserDao;
import com.kd.blog.entity.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;

import java.util.Date;

import static com.kd.blog.entity.User.CREATE_USER;
import static com.kd.blog.entity.User.FIND_USER_BY_NAME;

@Repository
public class UserDaoImpl extends GenericDaoHibernateImpl<User, Long> implements UserDao {

    @Override
    public User findUserByName(String name) {
        final TypedQuery<User> query = getDefaultEntityManager()
                .createNamedQuery(FIND_USER_BY_NAME, User.class)
                .setParameter(1, name);

        return query.getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Transactional
    public int createUser(String name) {
        return (int) getDefaultEntityManager()
                .createNamedQuery(CREATE_USER)
                .setParameter(1, name)
                .setParameter(2, new Date())
                .getSingleResult();
    }
}
