package com.kd.blog.dao.impl;

import com.kd.blog.dao.PostDao;
import com.kd.blog.entity.Post;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static com.kd.blog.entity.Post.*;

@Repository
public class PostDaoImpl extends GenericDaoHibernateImpl<Post, Long> implements PostDao {

    @Override
    public List<Post> findAll() {
        return getDefaultEntityManager()
                .createNamedQuery(FIND_ALL_POSTS, Post.class)
                .getResultList();
    }

    @Override
    public List<Post> findPostsByUser(Long id) {
        return getDefaultEntityManager()
                .createNamedQuery(FIND_POSTS_BY_USER, Post.class)
                    .setParameter(1, id)
                .getResultList();
    }

    @Override
    @Transactional
    public int createPost(String name, String description, Long authorId) {
        return getDefaultEntityManager()
                .createNamedQuery(INSERT_NEW_POST)
                    .setParameter(1, name)
                    .setParameter(2, description)
                    .setParameter(3, authorId)
                    .setParameter(4, new Date())
                .executeUpdate();
    }
}
