package com.kd.blog.dao.impl;

import com.kd.blog.dao.Dao;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

@Repository
public abstract class GenericDaoHibernateImpl<T, PK extends Serializable> implements Dao<T, PK> {

    @PersistenceContext
    private EntityManager entityManager;

    private Class<T> entityBean;

    public GenericDaoHibernateImpl() {
        //Getting the class that extends the GenericDaoHibernateImpl class
        this.entityBean = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public void save(T newInstance) {
        getDefaultEntityManager().persist(newInstance);
    }

    @Override
    public void update(T transientObject) {
        getDefaultEntityManager().merge(transientObject);
    }

    @Override
    public void delete(T persistentObject) {
        getDefaultEntityManager().remove(persistentObject);
    }

    @Override
    public void deleteById(PK id) {
        T persistentObject = findById(id);
        delete(persistentObject);
    }

    @Override
    public T findById(PK id) {
        return getDefaultEntityManager().find(getEntityBean(), id);
    }

    @Override
    public List<T> findAll() {
        return getDefaultEntityManager().createQuery("from " + entityBean.getName()).getResultList();
    }

    @Override
    public Optional<T> getById(PK id) {
        return Optional.ofNullable(findById(id));
    }

    @Override
    public Optional<List<T>> getAll() {
        return Optional.ofNullable(findAll());
    }

    @Override
    public void flush() {
        getDefaultEntityManager().flush();
    }

    @Override
    public void refresh(T entity) {
        getDefaultEntityManager().refresh(entity);
    }

    @Override
    public T merge(T entity) {
        return getDefaultEntityManager().merge(entity);
    }

    public Class<T> getEntityBean() {
        return entityBean;
    }

    public EntityManager getDefaultEntityManager() {
        return entityManager;
    }
}
