package com.kd.blog.dao;

import com.kd.blog.entity.Post;

import java.util.List;

public interface PostDao {

    List<Post> findAll();

    List<Post> findPostsByUser(Long id);

    int createPost(String name, String description, Long authorId);
}
