import React from 'react';
import Post from "./Post";

//Stateful component
export default class BlogContainer extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            postsList: [],
            isLoaded: false,
            username: '',
            postName: '',
            description: '',
            error: ''
        };

        //Bind events to the current component
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    //When this component is loaded, this function is fired.
    componentDidMount() {
        fetch('http://localhost:8080/posts', {
            headers: {'Content-Type': 'application/json'},
            method: 'GET', cache: 'default'
        }).then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Something went wrong... Could not get the response.');
            }
        }).then(data => {
            this.setState({isLoaded: true, postsList: data})
        });
    }

    //Handle the submit button event
    handleSubmit(event) {
        const {username, postName, description} = this.state;

        if(username.length === 0 || username.length > 40) {
            this.setState({error: 'Username is empty or exceeds 40 chars.'});
            event.preventDefault();
            return;
        }

        if(postName.length === 0 || postName.length > 40) {
            this.setState({error: 'Post name is empty or exceeds 40 chars.'});
            event.preventDefault();
            return;
        }

        if(description.length === 0 || description.length > 140) {
            this.setState({error: 'Description is empty or exceeds 140 chars.'});
            event.preventDefault();
            return;
        }

        fetch('http://localhost:8080/posts', {
            headers: {'Content-Type': 'application/json'},
            method: 'POST',
            cache: 'default',
            body: JSON.stringify({
                name: postName,
                description: description,
                author: {
                    name: username
                }
            })
        }).then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Something went wrong... Could not get the response.');
            }
        }).then(data => {
            this.setState({isLoaded: true, postsList: data})
        });
    }

    //When the field has changed, set the appropriate value to the state (username to state.username, postName to state.postName etc.)
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render() {
        const {postsList, isLoaded, error} = this.state;

        return(
            <div className="container blog-container">
                { error ?
                    <div className="alert alert-danger" role="alert">
                        {error}
                    </div>
                    : null}

                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>Username</label>
                        <input type="text" className="form-control" name="username" aria-describedby="emailHelp" placeholder="Enter your name" value={this.state.username} onChange={this.handleInputChange}/>
                    </div>
                    <div className="form-group">
                        <label>Post's name</label>
                        <input type="text" className="form-control" name="postName" placeholder="Enter the post's name" value={this.state.postName} onChange={this.handleInputChange}/>
                    </div>
                    <div className="form-group">
                        <label>Description</label>
                        <input type="text" className="form-control" name="description" placeholder="Enter the description" value={this.state.description} onChange={this.handleInputChange}/>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>

                {!isLoaded ? <p>Please wait.</p> :
                    <div className="container blog-container">
                        <div className="row">
                            { postsList.length ? postsList.map(item => <Post post={item} />) : <p>No posts here. Will yours be the first one?</p>}
                        </div>
                    </div>
                }
            </div>
        );
    }


}