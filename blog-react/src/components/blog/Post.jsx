import React from 'react';

//Stateless functional component of the post. Returns a post object with all information.
const Post = ({post}) => {

    return post ? (
        <div className="card col-12">
            <div className="card-body">
                <h2 className="card-title">{post.name}</h2>
                <p className="card-text">{post.description}</p>
            </div>
            <div className="card-footer text-muted">
                <p>Posted on {post.date} by {post.author.name}</p>
            </div>
        </div>
    ) : null;
};

export default Post;