import React from 'react';
import BlogHeader from "./blog/Header";
import BlogContainer from "./blog/Container";

import '../css/App.css';

function App() {
  return [
    <BlogHeader/>,
    <BlogContainer/>
  ];
}

export default App;
